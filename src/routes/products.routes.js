const router = require('express').Router();

const ProductsService = require('../services/product.service');
const service = new ProductsService();

router.get('/', async (req, res) => {
    // const products = [], {size} = req.query;
    // const limit = size || 5;

    // for (let index = 0; index < limit; index++) {
    //     products.push({
    //         name: faker.commerce.productName(),
    //         price: parseInt(faker.commerce.price(), 10),
    //         image: faker.image.imageUrl(),
    //     });

    // }

    const products = await service.find();

    res.json(products);
});

router.post('/', async (req, res) => {
    const body = req.body;

    const newProduct = await service.create(body);

    res.status(201).json(newProduct);
});

router.get('/:id', async (req, res, next) => {
    try {
        const {id} = req.params;
        const product = await service.findOne(id);
        res.json(product);

    } catch (error) {
        next(error);
    }
});

router.patch('/:id', async (req, res) => {
    try {
        const {id} = req.params;
        const body = req.body;

        const product = await service.update(id, body);

        res.json(product);

    } catch (error) {
        next(error);
    }

})

router.delete('/:id', async (req, res) => {
    const {id} = req.params;
    const rta = await service.delete(id);
    res.json(rta);
})

router.get('/filter', (req, res) => {
    res.send('Yo soy un filter');
});


module.exports = router;
