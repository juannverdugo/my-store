const router = require('express').Router();
const faker = require('faker');

router.get('/categories/:categoryId/products/:productId', (req, res) => {
    const {categoryId, productId} = req.params;
    res.send({
        categoryId,
        productId
    })
});

module.exports = router;
