const router = require('express').Router();

const productsRouter = require('./products.routes');
const userRouter = require('./user.routes');
const categoriesRouter = require('./categories.routes');

module.exports = (app) => {
    app.use('/api/v1', router);

    router.use('/products', productsRouter);
    router.use('/user', userRouter);
    router.use('/categories', categoriesRouter);
};
