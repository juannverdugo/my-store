const router = require('express').Router();
const faker = require('faker');

router.get('/users', (req, res) => {
    const {limit, offset} = req.query;
    if (limit && offset) {
        res.json({
            limit,
            offset
        });
    } else {
        res.send('No hay parametros');
    }
})

module.exports = router;
