const express = require('express');
const routerApi = require('./routes/index.routes');

const { logErrors, errorHandler, boomErrorHandler } = require('./middlewares/error.handler');

const app = express();

// settings
app.set('port', process.env.PORT || 3000);

// middlewares
app.use(express.json());

// routes
app.get('/', (req, res) => {
    res.send('Hola mi server en express');
});

routerApi(app);

app.use(logErrors)
app.use(boomErrorHandler)
app.use(errorHandler)

app.listen(app.get('port'), () => console.log('Server started'));
